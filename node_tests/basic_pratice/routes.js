const requestHandler = (req, res) => {
  const url = req.url;
  const method = req.method;
  const users = ['User 1', 'User 2', 'User 3', 'User 4'];

  if (url === '/' && method === 'GET') {
    res.setHeader('Content-Type', 'text/html')
    res.write('<html>');
    res.write('<head><title>Welcome Page</title></head>');
    res.write('<body><h1>Welcome</h1><p>Assigment 1</p>');
    res.write('<form action="create-user" method="POST">')
    res.write('<input name=username>');
    res.write('<button type="submit">Send</button>');
    res.write("<a href='/users'>List Users</a>");
    res.write('</form>');
    res.write('</body>');
    res.write('</html>');
    return res.end();
  }
  if (url === '/users' && method === 'GET') {
    res.setHeader('Content-Type', 'text/html')
    res.write('<html>');
    res.write('<head><title>Welcome Page</title></head>');
    res.write('<body><h1>User List</h1><ul>');
    for (let user in users) {
      res.write(`<li>${users[user]}</li>`);
    }
    res.write('</ul></body>');
    res.write("<a href='/ > Create User</a>");
    res.write('</html>');
    return res.end();
  }
  if (url === '/create-user' && method === "POST") {
    const body = [];
    req.on('data', (chunk) => {
      body.push(chunk);
    });
    return req.on('end', (err) => {
      const parsedBody = Buffer.concat(body).toString().split('=')[1];
      console.log(parsedBody);
      res.statusCode = 302
      res.setHeader('Location', `/users`);
      res.end();
    });
  }
};

exports.handler = requestHandler;