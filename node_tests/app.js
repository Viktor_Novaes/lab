const express = require('express');
const chalk = require('chalk')
const bodyParser = require('body-parser')

const app = express();
const server = require('http').Server(app);

app.use(bodyParser.urlencoded({extended: false}))

// app.get('/', (req, res) => {
//   res.send('Hay. Your Home Page Node + Express');
// })
app.get('/add-users', (req, res, next) => {
  res.send("<form action='/user' method='POST'><input type='text' name='username'><button type='submit'>send</button></form>")
})

app.post('/user', (req, res, next) => {
  console.log(req.body)
  res.redirect('/add-users')
})

app.get('/', (req, res, next) => {
  console.log('Home page express')
  res.redirect('/add-users')
})

server.listen(3000, () => {
  console.log(chalk.magenta('http://localhost:3000/ Server starting'));
});

// process.env.NODE_ENV

// process.on('SIGTERM', () => {
//   server.close(() => {
//     console.log('Process terminated')
//   })
// })

// process.argv.forEach((val, index) => {
//   console.log(`${index}: ${val}`)
// })

// const ProgressBar = require('progress')

// const bar = new ProgressBar(':bar', { total: 100 })
// const timer = setInterval(() => {
//   bar.tick()
//   if (bar.complete) {
//     clearInterval(timer)
//   }
// }, 100)
