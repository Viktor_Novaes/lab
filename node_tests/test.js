// Event Loop
  // const bar = () => console.log('bar')

  // const baz = () => console.log('baz')

  // const foo = () => {
  //   console.log('foo')
  //   setTimeout(() => {
  //     bar()
  //   }, 1000);
  //   setImmediate(() => {
  //     baz()
  //   })
  // }

  // foo()

// Class
  const dev = {
    name: 'Vitor',
    age: 21,
    sayHello() {
      console.log('Hey, How are you ?')
    }
  }

  dev.sayHello()

// Arrays
  const array = ['teste', 'teste1']
  console.log(array.map( element => {
    return 'This is: ' + element
  }))
  console.log(array.reverse())

const copiedDev = {...dev}
const copiedArray = [...array]
const mergeArray = [...array, ...copiedArray]

console.log(copiedDev)
console.log(copiedArray)
console.log(mergeArray)
