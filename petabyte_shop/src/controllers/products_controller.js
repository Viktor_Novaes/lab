const path = require('path');
const Product = require('../models/product');
const Department = require('../models/department');

// NEW
exports.newProduct = (req, res) => {
  res.status(200).sendFile(path.join(__dirname, '../', 'views', 'product', 'form.html'));
}

// CREATE
exports.createProduct = (req, res) => {
  if (req.body.name == null && req.body.price == null && req.body.quantity == null){
    res.status(400);
    res.end();
  } else {
    const product = new Product(
      {
        name: req.body.name, 
        price: req.body.price, 
        especification: req.body.especification, 
        quantity: req.body.quantity,
        departmentId: req.body.departmentId
      }
    );
    product.save().then(result => {
      console.log(result);
      const products = [];
      Department.findById(result.departmentId).then(department =>{
         products = department.products;
      });

      Department.updateOne({ where: result.departmentId },
        {
         products: products.push(result)
        }).then(department => {
          console.log(department);
          return department.save();
        });

        res.status(201);
        res.redirect('/product');
      }).catch(error => {
        console.log(error);
      });
  }
}

// INDEX
exports.getProducts = (req, res) => {
  res.setHeader('Content-Type', 'text/html');
  
  Product.find()
  .select() // Show attributes on Product
  .populate('departmentId', 'name level') // populate refs in document
  .then(products => {
    console.log(products);
    for (let product of products)
      res.write(`<i>category: ${product.departmentId.name}</i> <li>${product.name} R$ ${product.price} -> ${product.especification} : ( id: ${product._id})<button method='POST' action='/orders/add-product-to-order/${product._id}'>Add to Orders</button></li>`);
    res.end();
  }).catch(error => {
    console.log(error);
  });
}

// SHOW
exports.getProduct = (req, res) => {
  res.setHeader('Content-Type', 'text/html');
  const productID = req.params.productID;
  Product.findById(productID).then(result => {
    res.write(`<li>FIND -> ${result['name']} R$ ${result['price']} </li>`);
    res.end();
  }).catch(error => {
    console.log(error);
  });
}

// DELETE
exports.deleteProduct = (req, res) => {
  const productID = req.params.productID;
  Product.findByIdAndRemove(productID).then(result => { 
    console.log('DESTROYED');
    res.status(204).end();
  }).catch(error => {
    console.log(error);
  })
}

// EDIT
exports.editProduct = (req, res) => {
  res.setHeader('Content-Type', 'text/html');
  const productID = req.params.productID;
  Product.findAll({ where: { id: productID } }).then(result => {
    console.log(result[0])
    res.end();
  }).catch(error => {
    console.log(error);
  });
}

// PATCH
exports.pacthProduct = (req, res) => { 
  const id = req.params.productID
  Product.findById(id)
    .then(product => {
      product.name = req.body.name,
      product.especification = req.body.especification,
      product.price = req.body.price,
      product.quantity = req.body.quantity
      return product.save();
    }).then(result => {
      console.log('UPDATED');
      res.status(204);
      res.redirect('/product');
    }).catch(error => {
      console.log(error);
    });
}
