const path = require('path')

exports.getHomePage = (req, res, next) => {
  res.status(200).sendFile(path.join(__dirname, '../', 'views', 'shop', 'home.html'))
}