const Order = require('../models/order');
const Client = require('../models/client');

// INDEX
exports.getOrders = (req, res, next) => {
  Order.find()
    .populate('clientId')
    .then(result => {
      res.setHeader('Content-Type', 'text/html');
      for (let order of result) {
        res.write(`<li> -> Client: ${order.clientId.name} -> ${order._id} </li>`);
      }
      res.end();
    }).catch(error => console.log(error));
}

// NEW
exports.newOrder = (res, req, next) => {
  res.status(200).sendFile(path.join(__dirname, '../', 'views', 'order', 'form.html'));
}

// CREATE
exports.createOrder = (res, req, next) => {
  
  const order = new Order({
    clientId: req.body.clientId,
    product_order: []
  });
  
  order.save().then(result => {
    console.log(result)
    res.redirect('/orders');
    res.status(201);
  }).catch(error => {
    console.log(error);
  });
}

exports.addToOrder = (res, req) => {

}

// SHOW
exports.getOrder = (req, res) => {
  const id = req.params.orderId;
  Order.findById(id)
    .populate('product_order.items.productId')
    .execPopulate()
    .then(order => {
      console.log(order.product_order.items);
    })
    .catch(error => {
      console.log(error);
    });
}