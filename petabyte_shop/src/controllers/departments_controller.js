const Department = require('../models/department');

exports.getDepartments = (req, res) => {
  res.setHeader('Content-Type', 'text/html');
  
  Department
  .find()
  .populate('products.productId')
  .then(departments => {
    console.log(departments);
    for (let department of departments)
      res.write(`<li>${department['name']} -> ${department['level']} -> ${department['_id']}</li>`);
    res.end();
  }).catch( error => {
    console.log(error);
  });
}

exports.newDepartment = (req, res) => {

}

exports.createDepartment = (req, res) => {

}

exports.getDepartment = (req, res) => {

}