const Client = require('../models/client');
const path = require('path');

// INDEX
exports.getClients = (req, res, next) => {
  Client.find().populate('orders.items.orderId').then(result => {
    console.log(result);
    res.setHeader('Content-Type', 'text/html');
    for (let client of result) {
      res.write(`<li>${client.name}  -> ${client.email} -> ${client.id} </li>`);
    }
    res.end();
  }).cacth(error => console.log(error));
}

// NEW
exports.newClient = (res, req, next) => {
  res.status(200).sendFile(path.join(__dirname, '../', 'views', 'client', 'form.html'));
}

// CREATE
exports.createClient = (res, req, next) => {
  Client.create({
    name: req.body.name,
    email: req.body.email
  }).then(result => {
    console.log('create client');
    res.redirect('/clients');
    res.status(201);
  }).cacth(error => {
    console.log(error);
  });
}

// SHOW
exports.getClient = (req, res) => {
  const id = req.params.clientId;
  Client.findById(id)
    .populate('orders.items.orderId')
    .execPopulate()
    .then(result => {
      console.log(result);
    })
    .catch(error => {
      console.log(error);
    });
}