const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clientSchema = new Schema({
  name: {
    required: true,
    type: String
  },
  email: {
    required: true,
    type: String
  },
  orders: {
    items: [
      {
        orderId: {
          type: Schema.Types.ObjectId,
          ref: 'orders',
          required: true
        }
      }
    ]
  }
});

module.exports = mongoose.model('clients', clientSchema);


// const database = require('../../db/database');
// const Sequelize = require('sequelize');

// const client = database.define('client', {
//   id: {
//     type: Sequelize.INTEGER,
//     autoIncrement: true,
//     allowNull: false,
//     primaryKey: true,
//     field: 'id',
//   },
//   email: {
//     type: Sequelize.STRING,
//     field: 'email'
//   },
//   name : {
//     type: Sequelize.STRING,
//     field: 'name'
//   },
// });

// module.exports = client;