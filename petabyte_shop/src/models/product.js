const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  departmentId: {
    type: Schema.Types.ObjectId,
    ref: 'departments',
    required: true
  },
  especification: String,
});

module.exports = mongoose.model('products', productSchema);

// const db = require('../../db/database').db;
// class Product {
//   constructor(name, price, especification, quantity, id, departmentId) {
//     this.name = name;
//     this.price = price;
//     this.especification = especification;
//     this.quantity = quantity;
//     this._id = id ? new mongodb.ObjectID(id) : null;
//     this.departmentId = new mongodb.ObjectID(departmentId);
//   }
  
//   save() {
//     return db().collection('Products').insertOne(this).then(result => {
//       console.log(result);
//     }).catch(error => {
//       console.log(error);
//     })
//   }

//   update() {
//     return db().collection('Products').updateOne({_id: this._id, $set: this}).then(result => {
//       console.log(result);
//     }).catch(error => {
//       console.log(error);
//     })
//   }

//   static fecthAll() {
//     return db().collection('Products').find().toArray()
//       .then(products => {
//         console.log(products);
//         return products;
//       })
//       .catch(error => {
//         console.log(error);
//       });
//   }

//   static getProduct(id) {
//     return db().collection('Products').find({_id: new mongodb.ObjectId(id)}).next()
//       .then( result => {
//         console.log(result);
//         return result
//       })
//       .catch( error => {
//         console.log(error);        
//       })
//   }

//   static deletebyId(id) {
//     db().collection('Products').deleteOne({_id: new mongodb.ObjectID(id)})
//       .then(result => {
//         console.log('DELETE *');
//         return result;
//       })
//       .catch(error => {
//         console.log(error);
//         return error;
//       });
//   }
// }

// SETUP SEQUELIZE MYSQL

// const Sequelize = require('sequelize');

// const product = connection.define('product', {
//   id: {
//     type: Sequelize.INTEGER,
//     autoIncrement: true,
//     allowNull: false,
//     primaryKey: true,
//     field: 'id'
//   },
//   name: {
//     type: Sequelize.STRING,
//     field: 'name'
//   },
//   price: {
//     type: Sequelize.INTEGER,
//     field: 'price'
//   },
//   especification: {
//     type: Sequelize.STRING,
//     field: 'especification'
//   },
//   quantity: {
//     type: Sequelize.INTEGER,
//     field: 'quantity'
//   }
// });
