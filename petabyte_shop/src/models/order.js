const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema({
  clientId: {
    type: Schema.Types.ObjectId,
    ref: 'clients',
    required: true
  },
  product_order: {
    items: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: 'products',
          required: true
        },
        quantity: {
          type: Number,
          required: true
        }
      }
    ]
  },
});

orderSchema.methods.addToOrderList = function(product) {

}

module.exports = mongoose.model('orders', orderSchema);

// const database = require('../../db/database');
// const Sequelize = require('sequelize');

// const order = database.define('order', {
//   id: {
//     type: Sequelize.INTEGER,
//     autoIncrement: true,
//     allowNull: false,
//     primaryKey: true,
//     field: 'id',
//   },
//   total_price: Sequelize.FLOAT,
// });

// module.exports = order;