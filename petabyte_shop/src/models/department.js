const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const departmentSchema = new Schema({
  name: {
    required: true,
    type: String
  },
  level: {
    required: false,
    type: Number
  },
  products: [{
    product: Object
  }]
})


departmentSchema.methods.insertProduct = function(product) {
  this.products = [...product._id];
  this.save();
}

module.exports = mongoose.model('departments', departmentSchema);

// class Department {
//   constructor(name, level, id) {
//     this.name = name;
//     this.level = level;
//     this._id = id ? new mongodb.ObjectID(id) : null;
//   }

//   save() {
//     return db().collection('Departments').insertOne(this)
//       .then(result => { console.log(result) }).catch(error => { console.log(error) });
//   }

//   static fetchAll() {
//     return db().collection('Departments').find().toArray().then( result => {
//       console.log(result);
//       return result;
//     }).catch(error => {
//       console.log(error);
//     })
//   }
// }
