// Setup
const http = require('http')
const express = require('express')
const chalk = require('chalk')
const bodyParser = require('body-parser')
const hostname = '127.0.0.1'
const port = '3000'
const app = express()
const server = http.createServer(app)
const mongoose = require('mongoose');

mongoose
  .connect('mongodb+srv://usertest:GjIVaLWcyb3aima5@nodetest-hnadq.mongodb.net/shop?retryWrites=true&w=majority')
    .then(() => {
      console.log(chalk.green('MongoDB with Mongoose Connected'));
      server.listen(port, hostname, () => {
        console.log(chalk.magenta('http://localhost:3000/ Server starting'));
      });
    })
    .catch(error => {
      console.log(chalk.red(error));
    });

// routers
app.use(bodyParser.urlencoded({extended: false}));
const shopRouter = require('./config/routes/shop');
const productRouter = require('./config/routes/product');
const departmentRouter = require('./config/routes/department');
const orderRouter = require('./config/routes/order');
const clientRouter = require('./config/routes/client');

app.use(shopRouter);
app.use('/product', productRouter);
app.use('/department', departmentRouter);
app.use('/orders', orderRouter);
app.use('/clients', clientRouter);

// Not Found
app.use((req,res,next) => {
  res.status(404).send("<h1 style='color: red;'> 404 page not found")
})


// mongoConnect( () => {
//   console.log(chalk.green('MongoDB Connected'));
//   server.listen(port, hostname, () => {
//     console.log(chalk.magenta('http://localhost:3000/ Server starting'));
//   });
// })

// Models AND Relationships SQL
// const Client = require('./src/models/client');
// const Product = require('./src/models/product');
// const Order = require('./src/models/order');
// const productOrder = require('./src/models/product_order');

// Order.belongsTo(Client, { constraints: true, onDelete: 'CASCADE' });
// Client.hasMany(Order);
// Order.belongsToMany(Product, { through: productOrder });
// Product.belongsToMany(Order, { through: productOrder });

// // Database
// const sequelize = require('./db/database');
// // sequelize.sync({force: true}).then( // For create tables
// sequelize.sync().then(
//   server.listen(port, hostname, () =>{
//     console.log(chalk.magenta('http://localhost:3000/ Server starting'));
//   })
// ).catch( err => {
//   console.log(chalk.red(err));
// })
