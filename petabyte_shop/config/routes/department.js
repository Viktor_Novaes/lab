const express = require('express');
const router = express.Router();
const departmentController = require('../../src/controllers/departments_controller');

router.get('/', departmentController.getDepartments);
// router.get('/new', departmentController.newDepartment);
// router.post('/create', departmentController.createDepartment);
// router.get('/:departmentID', departmentController.getDepartment);

module.exports = router;