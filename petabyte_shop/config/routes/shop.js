const express = require('express')
const router = express.Router()
const shopController = require('../../src/controllers/shop_controller')

router.get('/home', shopController.getHomePage)

router.get('/', shopController.getHomePage)

module.exports = router