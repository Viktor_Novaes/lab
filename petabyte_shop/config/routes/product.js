const express = require('express');
const router = express.Router();
const productController = require('../../src/controllers/products_controller');

// Form new
router.get('/new', productController.newProduct);

// Create
router.post('/create', productController.createProduct);

// Index
router.get('/', productController.getProducts);

// Show
router.get('/:productID', productController.getProduct);

// Delete
router.delete('/delete/:productID', productController.deleteProduct);

// Patch
router.patch('/:productID', productController.pacthProduct);

module.exports = router;