const express = require('express');
const router = express.Router();
const clientController = require('../../src/controllers/clients_controller');

// Form new
// router.get('/new', clientController.newClient);

// Create
// router.post('/create', clientController.createClient);

// Index
router.get('/', clientController.getClients);

// // Show
router.get('/:clientId', clientController.getClient);

// // Patch
// router.patch('/:clientID', clientController.pacthClient);

// // Delete
// router.delete('/delete/:clientID', clientController.deleteClient);

module.exports = router;