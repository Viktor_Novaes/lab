const express = require('express');
const router = express.Router();
const orderController = require('../../src/controllers/orders_controller');

// Form new
// router.get('/new', orderController.newOrder);

// Create
// router.post('/create', orderController.createOrder);

// Index
router.get('/', orderController.getOrders);

// Add To Order
router.get('/add-product-to-order/:productId', orderController.addToOrder)

// Show
router.get('/:orderId', orderController.getOrder);

// // Patch
// router.patch('/:orderID', orderController.pacthOrder);

// // Delete
// router.delete('/delete/:orderID', orderController.deleteOrder);

module.exports = router;