
// const chalk = require('chalk');
// const Sequelize = require('sequelize');
// const sequelize = new Sequelize('Petabyte', 'usertest', 'password', {
//   dialect: 'mysql',
//   host: 'localhost',
//   port: 3306,
// })

// sequelize
//   .authenticate()
//   .then(() => {
//     console.log(chalk.green('Sequelize: Connection has been established successfully.'));
//   })
//   .catch(err => {
//     console.error('Sequelize: Unable to connect to the database:', err);
//   });

// module.exports = sequelize;

const MongoClient = require('mongodb').MongoClient;
let _db;

const mongoConnect = callback => {
  MongoClient.connect("mongodb+srv://usertest:GjIVaLWcyb3aima5@nodetest-hnadq.mongodb.net/shop?retryWrites=true&w=majority")
    .then(client => {
      console.log(client);
      _db = client.db();
      callback(client);
    }).catch( error => {
      console.log(error);
    });
}

const db = () => {
  if(_db)
    return _db;
  throw 'No database found';
}

exports.mongoConnect = mongoConnect;
exports.db = db;
