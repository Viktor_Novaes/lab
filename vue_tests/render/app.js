new Vue({
  el: '#app',
  data: {
    title: 'Altere esse texto',
    signed: false,
    colors: ['purple', 'blue', 'green', 'red', 'black', 'pink'],
    peoples: [
      { name: 'Vitor', age: 21 },
      { name: 'Negan', age: 45 }
    ]
  },
  methods: {
    changeTitle(event) {
      this.title = event.target.value;
    }
  },
})