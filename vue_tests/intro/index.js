new Vue({
  el: '#app',
  data: {
    title: 'Altere esse texto'
  },
  methods: {
    changeTitle(event) {
      this.title = event.target.value;
    }
  },
})