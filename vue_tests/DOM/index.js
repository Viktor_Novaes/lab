new Vue({
  el: '#app',
  data: {
    title: 'Altere esse texto',
    link: 'http://localhost:3000',
    linkTemplate: "<a href='http://localhost:3000'>Link render</a>",
    number: 0,
    x: 0,
    y: 0,
    increase_step: 1
  },
  methods: {
    changeTitle(event) {
      this.title = event.target.value;
    },
    increaseStep(event){
      this.increase_step = parseInt(event.target.value);
    },
    sum() {
      return this.number += this.increase_step;
    },
    mouseAttach(event) {
      this.x = event.clientX;
      this.y = event.clientY;
    },
    alertAction(event) {
      alert(event.target.value);
    }
  },
})