new Vue({
  el: '#desafio',
  data: {
    name: 'Vitor Novaes',
    age: '21'
  },
  methods: {
    ageMul() {
      return parseInt(this.age) * 3;
    },
    randomNumber() {
      return Math.random();
    }
  },
})